
package ListaCircularATSP;

/**
*La lista circular es una especie de lista enlazada simple o doblemente enlazada,
* pero que posee una característica adicional para el desplazamiento dentro de la lista: 
* esta no tiene fin. 
* Para que la lista sea sin fin, el puntero siguiente del último elemento apuntará hacia 
* el primer elemento de la lista en lugar de apuntar al valor NULL, 
* como hemos visto en el caso de listas enlazadas simples o doblemente enlazadas.
 * @author HP
 */
//se crea la clase CNodo
class CNodo {
    // creamos una variable de tipo entera llamada dato
	int dato;
        // creamos una variable de tipo CNodo llamada siguiente
	CNodo siguiente;
        //se crea el constructor de la clase
	public CNodo()	{
            //a la variable siguiente le asignamos el valor de null
            siguiente = null;
	}
}
//se crea la clase CLista
class CLista {
    // creamos una variable de tipo CNodo llamada cabeza
    CNodo cabeza;
    //se crea el constructor de la clase
    public CLista()	{
        //a la variable cabeza le asignamos el valor de null
            cabeza = null;
    }
//creamos el metodo Insertar Dato dentro de la clase CLista
    public void InsertarDato(int dat) {
        //se crea la variable NuevoNodo de tipo CNodo
        CNodo NuevoNodo;
        //y la variable antes y luego
        CNodo antes, luego;
        //se crea la sentencia new con la variable NuevoNodo para llamar a los objetos de la clase CNodo
        NuevoNodo = new CNodo();
        //se llama a la variable dato de la clase CNodo y se almacena en una nueva variable llamada dat
        NuevoNodo.dato=dat;
        // se crea la variable de tipo entero ban y se le asigna el valor de 0
        int ban=0;
        // este if pregunta si cabeza es igual a null, de ser así significa que está vacía y que será el primer dato que ingresemos 
        if (cabeza == null){
            //a cabeza se le asigna el NuevoNodo
            cabeza = NuevoNodo;
            //NuevoNodo.sguiente apunta a cabeza para así convertirse en circular
            NuevoNodo.siguiente=cabeza;
        }
        //si no está vacía significa que ya existen datos por lo tanto ingresa al else
         else {  
            // entre los casos tenemos si el dato que se ingresa es menor que el dato cabeza
            if (dat<cabeza.dato) {
                //si es menor entra en el if y a luego y antes le asigna el valor de cabeza
                    antes=cabeza;
                    luego=cabeza;
                    //luego a antes le asigna el valor de luego y a luego el valor de luego.siguiente
                    do{
                        antes=luego;
                        luego=luego.siguiente;
                        // el ciclo dice mientras luego sea diferente de cabeza se hace lo de arriba 
                    }while(luego!=cabeza);
                    //cuando luego se convierte en cabeza se hace lo siguiente
                    antes.siguiente=NuevoNodo;
                    cabeza = NuevoNodo;
                    NuevoNodo.siguiente=luego;
                }
            //en el caso de que el dato sea mayor que el dato cabeza se dirige al else
                else {  
                        // y se realiza lo siguiente a antes y luego le asigna el valor de cabeza
                        antes=cabeza;
                        luego=cabeza;
                        //se crea un ciclo que nos dice que mientras ban sea igual a cero entra y pregunta por las siguientes condiciones
                        while (ban==0){
                            //si el dato es mayor o igual que luego.dato
                            if (dat>=luego.dato) {
                                //se realiza lo siguiente
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            //si luego es igual que cabeza a la variable ban se le asigna el valor de 1
                            if (luego==cabeza){
                                ban=1;
                            }
                            //si luego no es igual que cabeza se realiza lo siguiente
                            else {
                                //pregunta si el dato es menor que luego.dato
                                    if (dat<luego.dato){
                                        //de ser así a la variable ban se le asigna el valor de uno caso contrario regresa al while
                                        ban=1;
                                    }
                            }
                        }
                        //luego de que se cumplan las condiciones del ciclo while cuando la variable ban tome el valor de 1 se realiza lo siguiente
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }
//creamos el metodo EliminarDato
    public void EliminarDato(int dat) {
        //Se crean las variables antes y luego de tipo CNodo
        CNodo antes,luego;
        //nuevamente a la variable ban le asignamos el valor de cero
        int ban=0;
        //se crean las siguietnes condiciones para saber que dato será eliminado
        //la primera condicion pregunta por el metodo vacía, si se cumple lo que este metodo dice entra en el if y muestra el siguietne mensaje
        if (Vacia()) {
            System.out.print("Lista vacía ");
        }
        //obviamente si no está vacía se dirige al else donde pregunta por mas condiciones
        else {  
            //si el dato que queremos eliminar es menor que el dato que está en cabeza
            if (dat<cabeza.dato) {
                //muestra un mensaje que nos indica que el dato no existe en la litsa
                    System.out.print("dato no existe en la lista ");
                }
            //si el dato no es menor obviamente se dirige al else donde encontramos los siguientes casos
                else {
                //si el dato es igual al dato que está en cabeza
                        if (dat==cabeza.dato) {
                            //y si cabeza es igual a cabeza.siguiente
                            if (cabeza==cabeza.siguiente){
                                //a cabeza se le asigna el valor de null
                                cabeza=null;
                            }
                            //si el dato no es igual al dato que está en cabeza se dirige al siguiente else
                            else{
                                //se realiza lo siguiente
                            antes=cabeza;
                            luego=cabeza;
                            //se crea un ciclo parecido al del metodo insertar para recorrer la lista y saber en que 
                            //lugar se encuentra el dato que queremos eliminar
                            do{
                                antes=luego;
                                luego=luego.siguiente;  
                            }
                            //el ciclo nos dice que mientras luego sea diferente de cabeza se realiza lo siguiente
                            while(luego!=cabeza);
                            cabeza=cabeza.siguiente;
                            antes.siguiente=cabeza;
                            }
                        }
                        //si el dato no es igual que el dato que se encuentra en cabeza se realiza lo siguiente
                        else {  antes=cabeza;
                                luego=cabeza;
                                //igual que en la parte de arriba se crea un ciclo para recorrer la lista y saber
                                //que elemento vamos a eliminar en la lista
                                while (ban==0) {
                                    //si el dato es mayor que luego.dato
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    //si luego es igual a cabeza
                                    if (luego==cabeza) {
                                        ban=1;
                                        System.out.print("numero inexistente ");
                                    }
                                    else {
                                        //si luego.dato es igual al dato
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                //si luego es igual a cabeza
                                if (luego==cabeza) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                    //si el dato es igual a luego.dato
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }
//se crea el metodo vacia
    public boolean Vacia() {
        //retorna que cabeza es igual a null
        return(cabeza==null);
    }
//se crea el metodo imprimir
    public void Imprimir() {
        //creamos una varible de tipo CNodo llamada Temporal
        CNodo Temporal;
        //A temporal se le asigna el valor de null
        Temporal=cabeza;
        // Si el metodo vacia es diferente es decir cabeza no tiene el valor de null se realiza lo siguiente
        if (!Vacia()) {
            //se crea un ciclo para recorrer la lista e irla mostrando
            do{
                System.out.print(" " + Temporal.dato +" ");
                Temporal = Temporal.siguiente;
            }while (Temporal!=cabeza);
            System.out.println("");
        }
        else
            //si se cumple lo que hay en el metodo vacia significa que la lista está vacía
            System.out.print("Lista vacía");
    }
}
//La clase principal donde se encuentra la clase main
public class ListaCircular {
     public static void main(String args[]) {
         //se crea una sentencia new con la variable objLista para llamar a los metodos u objeetos de la clase CLista
        CLista objLista= new CLista();
        objLista.InsertarDato(4);
        objLista.InsertarDato(2);
        objLista.InsertarDato(9);
        objLista.Imprimir();
        objLista.EliminarDato(2);
        objLista.Imprimir();
    }
}
